@extends('layouts.master')
@extends('layouts.BottomBar')

@section('content')

    {{--    Background--}}
    <div class="Background-Home"></div>

{{--    Placeholde search function doesnt work yet.--}}
<nav class="navbar fixed-top navbar-dark bg-dark">
    <div class="container-fluid justify-content-center">
        <form class="d-flex">
            <input class="form-control me-2" type="search" placeholder="Search for players" aria-label="Search">
            <button class="btn btn-outline-success" type="submit">Search</button>
        </form>
    </div>
</nav>


<div class="container">
    <div id="profile" class="row justify-content-center">
        <h1 id="Profile" class="h1 text-center">All Users</h1>
        {{--      LOCAL RANKING--}}
            {{--foreach loop to find all users in the database.--}}
            @foreach($Users as $User)
                <div class="col col-md-auto col-sm-auto col-xs-auto">
                    {{--this link wil get the ID and sent all the appropriate info to the individual page--}}
                    <a class="game" href="{{ route('user_detail', [ 'id'=>$User->id ]) }}">
                        <div id="Bars-card" class="card" style="width: 18rem;">
                            {{--showing the images from database--}}
                            <div class="card-body">
                                <h5 class="card-title">{{$User->name}}</h5> {{--showing the titles from the database--}}
                                <p class="card-text">wins: {{$User->wins}}</p> {{--showing the descriptions from the database--}}
                                {{--this link wil get the ID and sent all the appropriate info to the individual page--}}
                               <div class="card-footer">
                                <a href="{{ route('user_detail', [ 'id'=>$User->id ]) }}" class="btn btn-success">More info</a>
                               </div>
                               </div>
                        </div>
                    </a>
                </div>
            @endforeach
        </div>
</div>


@endsection



