@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-12 margin-tb">
                <div class="pull-left">
                    <h2>Voeg kroeg toe</h2>
                </div>
                <div class="pull-right">
                    <a class="btn btn-primary" href="{{ route('bars.index') }}"> Back</a>
                </div>
            </div>
        </div>

        @if ($errors->any())
            <div class="alert alert-danger">
                <strong>Whoops!</strong> There were some problems with your input.<br><br>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <form action="{{ route('bars.store') }}" method="POST" enctype="multipart/form-data">
            @csrf

{{--            <div class="row">--}}
{{--                <div class="col-xs-12 col-sm-12 col-md-12">--}}
{{--                    <div class="form-group">--}}
{{--                        <strong>Kroegnaam:</strong>--}}
{{--                        <input type="text" name="naam" class="form-control" placeholder="Kroegnaam">--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="col-xs-12 col-sm-12 col-md-12">--}}
{{--                    <div class="form-group">--}}
{{--                        <strong>Locatie:</strong>--}}
{{--                        <input type="text" name="locatie" class="form-control" placeholder="Locatie">--}}
{{--                    </div>--}}
{{--                </div>--}}

{{--                <div class="col-xs-12 col-sm-12 col-md-12">--}}
{{--                    <div class="form-group">--}}
{{--                        <strong>Description:</strong>--}}
{{--                        <input type="text" name="description" class="form-control" placeholder="Description">--}}
{{--                    </div>--}}
{{--                </div>--}}

{{--                <div class="col-xs-12 col-sm-12 col-md-12">--}}
{{--                    <div class="form-group">--}}
{{--                        <strong>Games:</strong>--}}
{{--                        <input type="text" name="tabletop_games_id" class="form-control" placeholder="Game">--}}
{{--                    </div>--}}
{{--                </div>--}}

{{--                <div class="col-xs-12 col-sm-12 col-md-12">--}}
{{--                    <div class="form-group">--}}
{{--                        <strong>Image:</strong>--}}
{{--                        <input type="file" name="image" class="form-control" placeholder="image">--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="col-xs-12 col-sm-12 col-md-12 text-center">--}}
{{--                    <button type="submit" class="btn btn-primary">Submit</button>--}}
{{--                </div>--}}
{{--            </div>--}}

        </form>
    </div>
@endsection

