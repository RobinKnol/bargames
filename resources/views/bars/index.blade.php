@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-lg-12 margin-tb">
                <div class="pull-left">
                    <h2>Bar registratie</h2>
                </div>
                <div class="pull-right">
{{--                    <a class="btn btn-success" href="{{ route('bars.create') }}"> Voeg Bar toe</a>--}}
                </div>
            </div>
        </div>

        @if ($message = Session::get('success'))
            <div class="alert alert-success">
                <p>{{ $message }}</p>
            </div>
        @endif

        <table class="table table-bordered">
            <tr>
                <th>ID</th>
                <th>Image</th>
                <th>Naam</th>
                <th>Locatie</th>
                <th>Description</th>
                <th>Games</th>
                <th>Details</th>
                <th width="280px">Action</th>
            </tr>
            @foreach ($bars as $bar)
                <tr>
                    <td>{{ ++$i }}</td>
                    <td><img src="/images/{{ $bar->image }}" width="100px"></td>
                    <td>{{ $bar->naam }}</td>
                    <td>{{ $bar->locatie }}</td>
                    <td>{{ $bar->description }}</td>
                    <td>games hier</td>
                    <td>
                        <form action="{{ route('bars.destroy',$bar->id) }}" method="POST">

                            <a class="btn btn-info" href="{{ route('bars.show',$bar->id) }}">Show</a>

                            <a class="btn btn-primary" href="{{ route('bars.edit',$bar->id) }}">Edit</a>

                            @csrf
                            @method('DELETE')

                            <button type="submit" class="btn btn-danger">Delete</button>
                        </form>
                    </td>
                    <td>{{ $bar->updated_at
                             ->format('d/m/Y') }}</td>
                </tr>
            @endforeach
        </table>
    </div>
    {{--    {!! $bar->links() !!}--}}

@endsection
