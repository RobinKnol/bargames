@extends('layouts.app')

<!DOCTYPE html>
<!-- QR code container -->
<div id="qrcode"></div>
<!-- text input field for user to enter text to encode into the QR code -->
<input type="text" id="text-input" placeholder="Voer hier in wat er in de QR code mot staan">

<!-- button to trigger QR code generation -->
<button id="generate-button">Genereer QR code</button>

<!-- button to download the generated QR code, initially disabled -->
<button id="download-button" disabled>Download QR code</button>

<!-- import the qrcode.min.js library to generate the QR code -->
<script src="https://cdn.discordapp.com/attachments/1066748496247525457/1066748682634002472/qrcode.min.js"></script>


<script>
    //create a new QR code and set its properties
    let qrcode = new QRCode(document.getElementById("qrcode"), {
        text: "",
        width: 256,
        height: 256,
        colorDark : "#000000",
        colorLight : "#ffffff"
    });

    //add an event listener to the generate button
    document.getElementById("generate-button").addEventListener("click", function(){
        //check if the input field is empty, if it is, display an alert
        if(document.getElementById("text-input").value === "") {
            alert("text veld is leeg, voer A.U.B. wat in.");
            return;
        }
        //generate the QR code using the text entered by the user
        qrcode.makeCode(document.getElementById("text-input").value);
        //enable the download button
        document.getElementById("download-button").disabled = false;
    });
    //add an event listener to the download button
    document.getElementById("download-button").addEventListener("click", function(){
        //get the canvas element containing the QR code image
        var canvas = document.querySelector('canvas');
        //convert the canvas to a data URL
        var dataURL = canvas.toDataURL();
        //create a link element
        var link = document.createElement('a');
        //set the download attribute to qrcode_BarGames.png
        link.download = "qrcode_BarGames.png";
        //set the href attribute to the data URL
        link.href = dataURL;
        //click the link to download the image
        link.click();
    });
</script>





















{{--<!DOCTYPE html>--}}
{{--<head>--}}
{{--    <title>QR code generator</title>--}}
{{--    <link rel="stylesheet" href="resources/css/scan.css">--}}
{{--    <script src="https://cdnjs.cloudflare.com/ajax/libs/quagga/0.12.1/quagga.min.js">--}}
{{--    <script src="https://unpkg.com/html5-qrcode/html5-qrcode.min.js" type="text/javascript">--}}
{{--    </script>--}}
{{--    <script src="{{ asset('node_modules\html5-qrcode\html5-qrcode.min.js') }}"></script>--}}

{{--</head>--}}
{{--<body>--}}
{{--<div id="qrcode"></div>--}}
{{--<input type="text" id="text-input" placeholder="Text to encode in QR code">--}}
{{--<button type="button" id="generate-button">Generate QR Code</button>--}}

{{--<script type="text/javascript">--}}
{{--    //tis here makes sure everything is loaded before loading te script to avoid errors--}}
{{--    window.onload = function () {--}}
{{--        document.getElementById("generate-button").addEventListener("click", function () {--}}
{{--            var inputText = document.getElementById("text-input").value;--}}
{{--            if (inputText === "") {--}}
{{--                alert("Input field is empty. Please enter a value.");--}}
{{--                return;--}}
{{--            }--}}
{{--            html5QrCode("qrcode", {--}}
{{--                text: inputText,--}}
{{--                width: 128,--}}
{{--                height: 128,--}}
{{--                colorDark: "#000000",--}}
{{--                colorLight: "#ffffff",--}}
{{--                correctLevel: QRCode.CorrectLevel.H--}}
{{--            });--}}
{{--        });--}}
{{--    }--}}
{{--</script>--}}
{{--</body>--}}

