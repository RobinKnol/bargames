@extends('layouts.app')

@section('content')

    {{--    <head>--}}
    {{--        <title>Spel Registratie</title>--}}
    {{--    </head>--}}
    <div class="container">


        <!-- form submit view -->
        <div class="container">
            @if(session()->has('success'))
                <script>
                    alert("{{ session()->get('success') }}");
                </script>
            @endif
        </div>


        <h1>Spel Registratie</h1>

        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        {!! Form::open(['route' => 'form.store']) !!}
        <div class="form-row">
            <div class="form-group col-6">
                {!! Form::label('email', 'Email') !!}
                {!! Form::email('email', null, ['class' => 'form-control', 'placeholder' => 'Voer het email adres waar je jouw bevestiging wilt ontvangen...']) !!}
            </div>
        </div>

        <!--Blade template-->

        <div class="form-row">
            <div class="form-group col-6">
                {!! Form::label('user_id', 'Naam') !!}
                {!! Form::select('user_id', $users->pluck('name', 'id'), null, ['class' => 'form-control', 'placeholder' => 'Selecteer de naam van de winnaar...']) !!}
            </div>
            <div class="form-group col-6">
                {!! Form::label('game_id', 'Spel') !!}
                {!! Form::select('game_id', $games->pluck('title', 'id'), null, ['class' => 'form-control', 'placeholder' => 'Selecteer het spel dat is gespeeld..']) !!}
            </div>
        </div>
        <div class="form-group col-6">
            {!! Form::label('amount_users', 'Aantal Spelers') !!}
            {!! Form::number('amount_users', null, ['class' => 'form-control','placeholder' => 'Selecteer het aantal spelers die mee hebben gespeeld..', 'min' => 1, 'max' => 10]) !!}
        </div>


        <div>
            <br>
            {!! Form::submit('Send', ['class' => 'btn btn-primary']) !!}
            {!! Form::close() !!}
        </div>

        <form action="{{ route('match.create') }}" method="post">
            @csrf
            <label for="winner">Winner</label>
            <select name="winner" id="winner">
                <option value="Player1">Player1</option>
                <option value="Player2">Player2</option>
            </select>
            <label for="loser">Loser</label>
            <select name="loser" id="loser">
                <option value="Player1">Player1</option>
                <option value="Player2">Player2</option>
            </select>
            <button type="submit">Save</button>
        </form>




@endsection
