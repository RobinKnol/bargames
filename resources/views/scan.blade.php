
@extends('layouts.master')
@extends('layouts.BottomBar')



    <!DOCTYPE html>
<head>
    <link rel="stylesheet" href="resources/css/scan.css">
    {{--    <script src="https://cdnjs.cloudflare.com/ajax/libs/quagga/0.12.1/quagga.min.js">--}}
    <script src="https://unpkg.com/html5-qrcode/html5-qrcode.min.js" type="text/javascript">
    </script>
</head>

@section('content')
    <!-- Create a container element for the QR code scanner -->
    <div id="reader"></div>

    <!-- Create an element to display the scanned QR code -->
    <div id="scanned-data">
    </div>
@endsection
<!-- Initialize the QR code scanner -->
<script>
    window.onload = function () {
        const scanner = new Html5QrcodeScanner('reader', {
            // Enable the mirror option to show the camera feed on screen
            qrbox: {
                width: 600,
                height: 600,
            },
            fps: 10,
        });

        scanner.render(success, error);

        // Starts scanner

        function success(result) {
            // Update the element to display the scanned QR code
            document.getElementById('scanned-data').innerHTML = `
      <h2>Success!</h2>
      <p><a href="${result}">${result}</a></p>
    `;

            scanner.clear();
            // Clears scanning instance

            document.getElementById('reader').remove();
            // Removes reader element from DOM since no longer needed
        }

        function error(err) {
            console.error(err);
            // Prints any errors to the console
        }
    }
</script>
