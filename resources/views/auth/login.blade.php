@extends('layouts.master')
{{--@extends('layouts.app')--}}


@section('content')
{{--    divs voor background animation--}}
    <div class="bg"></div>
    <div class="bg bg2"></div>
    <div class="bg bg3"></div>
{{--    end divs voor background animation--}}


    <div class="container Welcome">

        <div class="row justify-content-center">
            <div class="col col-md-auto col-sm-auto col-xs-auto">
                <div id="Image" class="LoginLogo">
                    <img src="https://static.cashbackxl.nl/Media/Webshops/Overig/logo_999games_cashback_xl.png" alt="" class="d-inline-block align-text-top">
                </div>
            </div>
        </div>

        <div class="row justify-content-center">
            <div class="col col-md-auto col-sm-auto col-xs-auto">
                <div id="title">
                    <h1 class="h1">Welcome!</h1>
                </div>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col col-md-auto col-sm-auto col-xs-auto">
                <div id="sub-title">
                    <h3 class="h3">Please login!</h3>
                </div>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-sm-9 col-xs-auto">
                <div id="login" class="login">

                    <form method="POST" action="{{ route('login') }}">
                        @csrf
                        <label for="email" class="col-md-4 col-form-label text-md-start">{{ __('E-mail Adres') }}</label>
                        <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                        @error('email')
                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                </span>
                        @enderror

                        <label for="password" class="col-md-4 col-form-label text-md-start">{{ __('Wachtwoord') }}</label>
                        <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                        @error('password')
                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                </span>
                        @enderror

                        <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                        <label class="form-check-label" for="remember">
                            {{ __('Remember Me') }}
                        </label>
                        <div class="row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Login') }}
                                </button>

                                @if (Route::has('password.request'))
                                    <a class="btn btn-link" href="{{ route('password.request') }}">
                                        {{ __('Wachtword vergeten?') }}
                                    </a>
                                @endif
                            </div>
                            <div class="row justify-content-center">
                                <div class="col-md-auto col-sm-auto">
                                    <div id="Register">
                                        <a type="button" class="btn btn-link" href="/Register">Lid worden? Meld je nu snel aan!</a>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
