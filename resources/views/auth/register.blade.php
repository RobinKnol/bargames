@extends('layouts.master')



@section('content')
    {{--divs voor background animation--}}
    <div class="bg"></div>
    <div class="bg bg2"></div>
    <div class="bg bg3"></div>
    {{--end divs voor background animation--}}


    <div class="container Welcome">



        <div class="row justify-content-center">
            <div class="col col-md-auto col-sm-auto col-xs-auto">
                <div id="Image" class="LoginLogo">
                    <img src="https://static.cashbackxl.nl/Media/Webshops/Overig/logo_999games_cashback_xl.png" alt="" class="d-inline-block align-text-top"></a>
                </div>
            </div>
        </div>

        <div class="row justify-content-center">
            <div class="col col-md-auto col-sm-auto col-xs-auto">
                <div id="title">
                    <h1 class="h1">Welcome!</h1>
                </div>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col col-md-auto col-sm-auto col-xs-auto">
                <div id="sub-title">
                    <h3 class="h3">Please register to continue!</h3>
                </div>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-sm-9 col-xs-auto">
                <div id="login" class="login">

                    <form method="POST" action="{{ route('register') }}">
                        @csrf

                        <div class="row mb-3">
                            <label for="name" class="col-md-4 col-form-label text-md-end">{{ __('Uw speler naam') }}</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                                @error('name')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="row mb-3">
                            <label for="email" class="col-md-4 col-form-label text-md-end">{{ __('E-mail adres') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

                                @error('email')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="row mb-3">
                            <label for="password" class="col-md-4 col-form-label text-md-end">{{ __('Wachtwoord') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                                @error('password')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="row mb-3">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-end">{{ __('Verifieer wachtwoord') }}</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                            </div>
                        </div>
                        <form action="{{route('register')}}" method="POST">
                            @csrf
                            <div class="form-group">
                                <label for="city">City</label>
                                <select name="city" id="city" class="form-control">
                                    <option value="Amsterdam">Amsterdam</option>
                                    <option value="Rotterdam">Rotterdam</option>
                                    <option value="The Hague">The Hague</option>
                                    <option value="Utrecht">Utrecht</option>
                                </select>
                            </div>
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </form>



                        <div class="row mb-0 text-center
                        ">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Registreer') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>




    </div>
@endsection

