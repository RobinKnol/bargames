{{--this is the first thing people see when they login, from here they can start a game--}}
@extends('layouts.master')
@extends('layouts.BottomBar')


@section('content')
    {{--divs voor background animation--}}
    <div class="bg"></div>
    <div class="bg bg2"></div>
    <div class="bg bg3"></div>
    {{--end divs voor background animation--}}


<div class="container Welcome">
    <div class="row justify-content-center">
        <div class="col col-md-auto col-sm-auto col-xs-auto">
            <div id="Image" class="LoginLogo">
                <img src="https://static.cashbackxl.nl/Media/Webshops/Overig/logo_999games_cashback_xl.png" alt="" class="d-inline-block align-text-top"></a>
            </div>
        </div>
    </div>
    <div class="row justify-content-center">
        <div class="col col-md-auto col-sm-auto col-xs-auto">
            <div id="title">
                <h1 class="h1">Welcome!</h1>
            </div>
        </div>
    </div>
    <div class="row justify-content-center">
        <div class="col col-md-auto col-sm-auto col-xs-auto">
            <div id="sub-title">
            <h3 class="h3">Want to play a game?</h3>
            </div>
        </div>
    </div>
    <div id="welcome" class="row justify-content-center">
        <div class="col-md-auto col-sm-auto col-xs-auto">
            <div id="loginButton">
                <a class="btn btn-lg btn-success" href="/Games" role="button"><i class="fa-solid fa-beer-mug-empty"></i> Play a game! <i class="fa-solid fa-beer-mug-empty"></i></a>
            </div>
        </div>
    </div>
    <div class="row justify-content-center">
        <div class="col-md-auto col-sm-auto">
            <div id="Register">
                <a class="btn btn-outline-success" href="/Bars" role="button"><i class="fa-solid fa-dice"></i> Find our bars <i class="fa-solid fa-dice"></i></a>
            </div>
        </div>
    </div>
</div>
@endsection
