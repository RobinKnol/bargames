@extends('layouts.master')
@extends('layouts.BottomBar')

@section('content')

    {{--    Background--}}
    <div class="Background-Home"></div>
    {{--    Placeholder search system doesn't work yet--}}
    <nav class="navbar fixed-top navbar-dark bg-dark">
        <div class="container-fluid justify-content-center">
            <form class="d-flex">
                <input class="form-control me-2" type="search" placeholder="Search for players" aria-label="Search">
                <button class="btn btn-outline-success" type="submit">Search</button>
            </form>
        </div>
    </nav>

    <div class="container">
        <div id="User" class="row">
            <h1 class="text-center Userheader">Welcome {{Auth::user()->name}}</h1>
            <hr style="height:2px;max-width: inherit; color:gray;background-color:gray">
            <div class="col">
                {{--            Get user profile picture from database--}}
                <div id="User" class="Profilepic">
                    <img src="https://ps.w.org/user-avatar-reloaded/assets/icon-256x256.png?rev=2540745" alt=""
                         class="d-inline-block align-text-top Profilepic">
                </div>
            </div>
            <div class="col-8">
                <p>Name: {{Auth::user()->name}}</p>
                <p>E-mail: {{Auth::user()->email}}</p>
            </div>
        </div>

        <div id="Uhistory" class="row text-center">
            <h2 class="text-center">{{Auth::user()->name}} scores</h2>
            <div class="col">
                <div id="User" class="card">
                    <div class="card-body">
                        <h5 class="card-title">Win/Loss Ratio</h5>
                        <p>Wins: {{ Auth::user()->wins }}</p>
                        <p>Losses: {{ Auth::user()->losses }}</p>
                        <p>Ratio:
                            @if(Auth::user()->losses == 0)
                                {{ Auth::user()->wins }}
                            @else
                                {{ Auth::user()->wins / Auth::user()->losses }}
                            @endif
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <div id="Uhistory" class="row text-center">
            <div class="col">
                <div class="logout">
                    <a class="btn btn-lg btn-success" href="{{ route('logout') }}"
                       onclick="event.preventDefault();
                    document.getElementById('logout-form').submit();"><i class="fa-solid fa-arrow-right-from-bracket"></i>
                        {{ __('Logout') }}
                    </a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                        @csrf
                    </form>
                </div>
            </div>
        </div>


    </div>
    </div>
@endsection

