@extends('layouts.master')
@extends('layouts.BottomBar')

@section('content')

    {{--    Background--}}
<div class="Background-Home"></div>
    <nav class="navbar fixed-top navbar-dark bg-dark">
        <div class="container-fluid justify-content-center">
            <form class="d-flex">
                <input class="form-control me-2" type="search" placeholder="Search for players" aria-label="Search">
                <button class="btn btn-outline-success" type="submit">Search</button>
            </form>
        </div>
    </nav>

<div class="container">
    <div id="User" class="row">
        <h1 class="text-center Userheader">{{ $User->name }}</h1>
        <hr style="height:2px;max-width: inherit; color:gray;background-color:gray">
        <div class="col">
            {{--            Get user profile picture from database--}}
            <div id="User" class="Profilepic">
                <img src="https://ps.w.org/user-avatar-reloaded/assets/icon-256x256.png?rev=2540745" alt="" class="d-inline-block align-text-top Profilepic">
            </div>
        </div>
        <div class="col">
            <h1 class="text-center">{{ $User->name }}</h1>
        </div>
    </div>
    <div id="Uhistory" class="row text-center">
        <h2 class="text-center">{{$User->name}} scores</h2>
        <div class="col">
            <div id="User" class="card">
                <div class="card-body">
                    <h5 class="card-title">Win/Loss Ratio</h5>
                    <p>Wins: {{ $User->wins }}</p>
                    <p>Losses: {{ $User->losses }}</p>
                    <p>Ratio:
                        @if($User->losses == 0)
                            {{ $User->wins }}
                        @else
                            {{ $User->wins / Auth::user()->losses }}
                        @endif
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

