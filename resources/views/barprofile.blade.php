{{--this file wil show individual bars when clicked on from the list.--}}

@extends('layouts.master')
@extends('layouts.BottomBar')
@section('content')
    {{--Background--}}
<div class="Background-Home"></div>

    {{--this is the info for the bar--}}
<div class="container">
    <div id="User" class="row">
        <h1 class="text-center Userheader">{{ $Bar->naam }}</h1> {{--getting the bar name--}}
        <hr style="height:2px;max-width: inherit; color:gray;background-color:gray">
        <div class="col">
                {{--            Get user profile picture from database--}}
            <div class="card">
                <div class="row g-0">
                    <div class="col col-sm-3 col-md-3">
                        <img src="{{ Storage::url($Bar->image) }}" class="img-fluid rounded-start" alt="..."> {{--getting the image from the bar--}}
                    </div>
                    <div class="col-md-6">
                        <div class="card-body">
                            <h5 class="card-title">{{ $Bar->naam }}</h5>
                            <hr style="height:2px;max-width: inherit; color:gray;background-color:gray">
                            <p class="card-text">{{ $Bar->description }}</p> {{--getting the descriptive info from the bar--}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

        {{--here the games available in the bar will show--}}
    <div id="BarPage" class="row justify-content-center">
        <h1 class="h1-Bars text-center">Our games:</h1>
        @foreach($Bar->Tabletopgame as $Game) {{--showing what games the bar has--}}
            <div id="User" class="card mb-3" style="max-width: 540px;">
                <div class="row g-0">
                    <div class="col-md-4">
                        <img id="BarPage" src="{{ Storage::url($Game->image) }}" class="img-fluid rounded-start" alt="{{ Storage::url($Game->image) }}"> {{--image from the game--}}
                    </div>
                    <div class="col-md-8">
                        <div class="card-body">
                         <h5 class="card-title">{{ $Game->title }}</h5> {{--showing the game name--}}
                        <p class="card-text">{{ $Game->description }}</p> {{--showing descriptive text--}}
                        <a href="/GameSession" class="btn btn-success">Start a game!</a> {{--to start a game--}}
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
    <div id="BarPage" class="row justify-content-center">
        <h1 class="h1-Bars text-center">Our score:</h1>
        <div id="User" class="card mb-3" style="max-width: 540px;">
            <div class="row g-0">
                <div class="col-md-8">
                    <div class="card-body">
                        <p class="card-text">{{ $Bar->score }}</p> {{--showing descriptive text--}}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

