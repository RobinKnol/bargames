{{--this page will display all games that are currently in the database.--}}
@extends('layouts.master')
@extends('layouts.BottomBar')

@section('content')

    {{--    Background--}}
<div class="Background-Home"></div>

{{--Placeholder searchbar doesn't work yet--}}
<nav class="navbar fixed-top navbar-dark bg-dark">
    <div class="container-fluid justify-content-center">
        <form class="d-flex">
            <input class="form-control me-2" type="search" placeholder="Search" aria-label="Search">
            <button class="btn btn-outline-success" type="submit">Search</button>
        </form>
    </div>
</nav>

{{--    here the actual content begins.--}}
<div class="container ">
    <div id="Bars" class="row">
        <h1 class="h1-Bars text-center">Our Games:</h1>
        {{--foreach loop to find all games in the database.--}}
        @foreach($TabletopGames as $TabletopGame)
        <div class="col col-md-auto col-sm-auto col-xs-auto">
            {{--this link wil get the ID and sent all the appropriate info to the individual page--}}
            <a class="game" href="{{ route('game_detail', [ 'id'=>$TabletopGame->id ]) }}">
             <div id="Bars-card" class="card" style="width: 18rem;">
                 {{--showing the images from database--}}
                 <img src="{{ Storage::url ($TabletopGame->image) }}" class="d-inline-block border text-center rounded" style="max-height: 190px;" alt="{{ $TabletopGame->image }}">
                 <div class="card-body">
                    <h5 class="card-title">{{ $TabletopGame->title }}</h5> {{--showing the titles from the database--}}
                    <p class="card-text">{{ $TabletopGame->description }}</p> {{--showing the descriptions from the database--}}
                     {{--this link wil get the ID and sent all the appropriate info to the individual page--}}
                    <a href="{{ route('game_detail', [ 'id'=>$TabletopGame->id ]) }}" class="btn btn-success">More info</a>
                 </div>
             </div>
             </a>
        </div>
        @endforeach
    </div>
{{--    Link to QR-scanner for individual games--}}
    <div id="Home1" class="row justify-content-center">
        <div class="col col-md-auto col-sm-auto col-xs-auto">
            <div class="title">
                <p class="text-center">couldn't find what you're looking for?</p>
                <div id="stuff" class="d-grid gap-2 col-6 mx-auto">
                    <a href="/scan" class="btn btn-success">Or scan a QR-code</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

