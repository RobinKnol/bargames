


<nav class="navbar navbar-expand-lg  bg-dark fixed-bottom navbar-dark justify-content-center">
    <!-- Container wrapper -->
    <div class="container-fluid justify-content-center">
        <a class="navbar-brand" href="/home">
            <img src="https://static.cashbackxl.nl/Media/Webshops/Overig/logo_999games_cashback_xl.png" alt="" width="40" height="30" class="d-inline-block align-text-top"></a>

        <!-- Icons -->
        <ul class="navbar-nav d-flex flex-row me-1">
            <li class="nav-item me-3 me-lg-0">
                <a class="nav-link mx-1 text-white" href="/User"><i class="fa-solid fa-user"></i></a>
            </li>
            <li class="nav-item me-3 me-lg-0">
                <a class="nav-link mx-1 text-white" href="/Users"><i class="fa-solid fa-address-book"></i></a>
            </li>
            <li class="nav-item me-3 me-lg-0 ">
                <a class="nav-link mx-1 text-white " href="/Bars"><i class="fa-solid fa-mug-saucer"></i></a>
            </li>
            <li class="nav-item me-3 me-lg-0">
                <a class="nav-link mx-1 text-white" href="/Games"><i class="fa-solid fa-dice"></i></a>
            </li>
            <li class="nav-item me-3 me-lg-0">
                <a class="nav-link mx-1 text-white" href="/Leaderboard"><i class="fa-solid fa-ranking-star"></i></a>
            </li>
            <li class="nav-item me-3 me-lg-0">
                <a class="nav-link mx-1 text-white" href="/nieuws"><i class="fa-brands fa-elementor"></i></a>
            </li>
        </ul>
    </div>
</nav>



