@extends('layouts.master')
@extends('layouts.BottomBar')

@section('content')

    {{--    Background--}}
    <div class="Background-Home"></div>
    <nav class="navbar fixed-top navbar-dark bg-dark">
        <div class="container-fluid justify-content-center">
            <form class="d-flex">
                <input class="form-control me-2" type="search" placeholder="Search for players" aria-label="Search">
                <button class="btn btn-outline-success" type="submit">Search</button>
            </form>
        </div>
    </nav>


    <div class="container">



    <div id="Leader" class="row justify-content-center">
    <h1 class="h1-Bars text-center">All rankings</h1>
    <div id="Leader" class="card" style="width: 18rem;">
        <div class="card-header">
            World ranking
        </div>
        @foreach ($users as $user)
{{--            @dd($user)--}}
            <ol class="list-group list-group">
                {{--            @foreach() while $rank <10 TO SHOW MAX 10 RESULTS--}}
                <li class="list-group-item d-flex justify-content-between align-items-start">
                    <div class="ms-2 me-auto">
                        <div class="fw-bold">{{ $user->name }}</div>
                        {{--                    REPLACE SCORE WITH DATABASE SCORE--}}

                        {{ $user->wins }}
                    </div>
                </li>
                @endforeach
            </ol>

            <div class="card-footer">
                <a href="#" class="btn btn-success">More info</a>
            </div>

    </div>

    {{--      BARRANKING--}}
    <div id="Leader" class="card" style="width: 18rem;">
        <div class="card-header">
            Bar ranking
        </div>
        @foreach ($bars as $bar)
            <ol class="list-group list-group">
                {{--             @foreach ($scorebord->bar as $barscore)--}}
                <li class="list-group-item d-flex justify-content-between align-items-start">
                    <div class="ms-2 me-auto">
                        {{--                      @dd($bar)--}}
                        {{--                      {{$scorebord->title}}--}}
                        <div class="fw-bold">{{ $bar->naam }}</div>
                        {{--                    REPLACE SCORE WITH DATABASE SCORE--}}
                        {{ $bar->wins }}
                    </div>
                </li>
                @endforeach
            </ol>
            <div class="card-footer">
                <a href="#" class="btn btn-success">More info</a>
            </div>
    </div>

    {{--      LOCAL RANKING--}}
    <div id="Leader" class="card" style="width: 18rem;">
        <div class="card-header">
            Game ranking
        </div>
        @foreach ($game as $games)
            <ol class="list-group list-group">
                {{--            @foreach() while $rank <10 TO SHOW MAX 10 RESULTS--}}
                <li class="list-group-item d-flex justify-content-between align-items-start">
                    <div class="ms-2 me-auto">
                        {{--                    REPLACE NAME WITH DATABASE NAME--}}
                        <div class="fw-bold">{{ $games->title }}</div>
                        {{--                    REPLACE SCORE WITH DATABASE SCORE--}}
                        {{ $games->wins }}
                    </div>
                </li>
                @endforeach
            </ol>
            <div class="card-footer">
                <a href="#" class="btn btn-success">More info</a>
            </div>
    </div>
</div>
        <div class="row">
            <div class="col">
                <h2>Win/Loss Ratios</h2>
                <table class="table">
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>Wins</th>
                        <th>Losses</th>
                        <th>Ratio</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($users as $user)
                        <tr>
                            <td>{{ $user->name }}</td>
                            <td>{{ $user->wins }}</td>
                            <td>{{ $user->losses }}</td>
                            <td>{{ $user->wins / ($user->losses ?: 1) }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>

</div>
@endsection

