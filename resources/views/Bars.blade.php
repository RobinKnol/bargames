{{--This file shows all bars that currently are in the database--}}
@extends('layouts.master')
@extends('layouts.BottomBar')

@section('content')

    {{--    Background--}}
<div class="Background-Home"></div>

    {{--placeholder search system that doesnt work yet.--}}
<nav class="navbar fixed-top navbar-dark bg-dark">
    <div class="container-fluid justify-content-center">
        <form class="d-flex">
            <input class="form-control me-2" type="search" placeholder="Search" aria-label="Search">
            <button class="btn btn-outline-success" type="submit">Search</button>
        </form>
    </div>
</nav>

<div class="container">
    <div id="Bars" class="row justify-content-center">
        <h1 class="h1-Bars text-center">Our bars:</h1>
            @foreach($Bars as $Bar) {{--Foreachloop to show all the bars in the database--}}
                <div  id="Bars-card" class="col col-md-auto col-sm-auto col-xs-auto">
                    <div id="Bars-card" class="card" style="width: 18rem;">
                        <img src="{{ Storage::url($Bar->image) }}" class="d-inline-block border text-center rounded" style="height: 190px;" alt="{{ $Bar->image }}"> {{--finding the image--}}
                            <div class="card-body">
                                <h5 class="card-title">{{ $Bar->naam }}</h5> {{--finding the name--}}
                                <p class="card-text">{{ $Bar->description }}</p> {{--finding the description--}}
                                {{--This link will find the ID of the bar and send it to the Barprofile.blade.php so show individual pages for each bar--}}
                                <a  href="{{ route('bar_detail', [ 'id'=>$Bar->id ]) }}" class="btn btn-success" style="margin-bottom: 10px;">{{ $Bar->naam }}</a>
                                <div class="card-footer text-muted">
                                    {{ $Bar->locatie }} {{--finding the location of the bar--}}
                                </div>

                            </div>
                    </div>
                </div>
           @endforeach
    </div>

{{--  Placeholder for when we're inplementing a map feature!  --}}
    <div id="Home1" class="row justify-content-center">
        <div class="col col-md-auto col-sm-auto col-xs-auto">
            <div class="title">
                    <p class="text-center">couldn't find what you're looking for?</p>
                <div id="stuff" class="d-grid gap-2 col-6 mx-auto">
                    <button class="btn btn-success" type="button">Open the map!</button>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
