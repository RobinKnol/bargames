@extends('layouts.master')
@extends('layouts.BottomBar')
<script>
    $(document).ready(function(){

        $("button.plus").on("click",function(){

            $("div:last").after("<div class=item><p>Title</p></div>");

        })

    })
</script>

@section('content')

    {{--divs voor background animation--}}
    <div class="bg"></div>
    <div class="bg bg2"></div>
    <div class="bg bg3"></div>
    {{--end divs voor background animation--}}

    <div id="Welcome" class="container">
        <div class="row justify-content-center">
            <div class="col col-md-auto col-sm-auto col-xs-auto">
                <div id="Image" class="LoginLogo">
                    <img src="https://static.cashbackxl.nl/Media/Webshops/Overig/logo_999games_cashback_xl.png" alt="" class="d-inline-block align-text-top"></a>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col text-center">
                <h1>Spel Registratie</h1>

            </div>
        </div>
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        {!! Form::open(['route' => 'form.store']) !!}
        <div class="row justify-content-center">
            <div class="form-group col-6">
                {!! Form::label('email', 'Email') !!}
                {!! Form::email('email', null, ['class' => 'form-control', 'placeholder' => 'Voer het email adres waar je jouw bevestiging wilt ontvangen...']) !!}
            </div>
        </div>

        <!--Blade template-->

        <div class="row">
            <div class="form-group col-4">
                {!! Form::label('user_id', 'Naam') !!}
                {!! Form::select('user_id', $users->pluck('name', 'id'), null, ['class' => 'form-control', 'placeholder' => 'Selecteer de naam van de winnaar...']) !!}
            </div>
            <div class="form-group col-4">
                {!! Form::label('game_id', 'Spel') !!}
                {!! Form::select('game_id', $games->pluck('title', 'id'), null, ['class' => 'form-control', 'placeholder' => 'Selecteer het spel dat is gespeeld..']) !!}
            </div>
            <div class="form-group col-4">
                {!! Form::label('bar_id', 'bar') !!}
                {!! Form::select('bar_id', $bar->pluck('naam', 'id'), null, ['class' => 'form-control', 'placeholder' => 'Selecteer de bar waar je nu speelt']) !!}
            </div>
        </div>

        <form method="post" action="{{ route('form.store') }}">
            @csrf
            <div class="form-group col-md4">
                <label for="winner">Winner</label>
                <select name="winner" id="winner" class="form-control">
                    @foreach($users as $user)
                        <option value="{{ $user->id }}">{{ $user->name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group col-md4">
                <label for="loser">Loser</label>
                <select name="loser[]" id="loser" class="form-control">
                    @foreach($users as $user)
                        <option value="{{ $user->id }}">{{ $user->name }}</option>
                    @endforeach
                </select>
            </div>
            <div id='form' class="row">
            <div class="form-group" id="loser-fields"></div>
            <div class="form-group col-md4">
                <label for="amount_of_losers">Amount of losers</label>
                <input type="number" name="amount_of_losers" id="amount_of_losers" class="form-control" min="1" max="5" >
                <input type="submit" value="Save" class="btn btn-primary">
            </div>
    </div>
        </form>

        <script>
            document.querySelector('#amount_of_losers').addEventListener('change', function(){
                var amount_of_losers = this.value;
                var loserFields = document.querySelector('#loser-fields');
                loserFields.innerHTML = '';
                for(var i = 1; i < amount_of_losers; i++){
                    var newLoserField = document.querySelector('#loser').cloneNode(true);
                    newLoserField.name = 'loser[]';
                    newLoserField.id = 'loser' + i;
                    loserFields.appendChild(newLoserField);
                }
            });
        </script>

        <div class="row justify-content-center">
            <div class="col-md4 text-center">
                <br>
                {!! Form::submit('Send', ['class' => 'btn btn-primary']) !!}
                {!! Form::close() !!}
            </div>

        </div>
    </div>

@endsection









{{--@extends('layouts.master')--}}
{{--@extends('layouts.BottomBar')--}}
{{--<script>--}}
{{--    $(document).ready(function(){--}}

{{--        $("button.plus").on("click",function(){--}}

{{--            $("div:last").after("<div class=item><p>Title</p></div>");--}}

{{--        })--}}

{{--    })--}}
{{--</script>--}}

{{--@section('content')--}}
{{--    --}}{{--divs voor background animation--}}
{{--    <div class="bg"></div>--}}
{{--    <div class="bg bg2"></div>--}}
{{--    <div class="bg bg3"></div>--}}
{{--    --}}{{--end divs voor background animation--}}

{{--    <div class="container Welcome">--}}
{{--        <div class="row justify-content-center">--}}
{{--            <div class="col col-md-auto col-sm-auto col-xs-auto">--}}
{{--                <div id="Image" class="LoginLogo">--}}
{{--                    <img src="https://static.cashbackxl.nl/Media/Webshops/Overig/logo_999games_cashback_xl.png" alt="" class="d-inline-block align-text-top"></a>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--<div class="row">--}}
{{--    <div class="col text-center">--}}
{{--        <h1>Spel Registratie</h1>--}}
{{--    </div>--}}
{{--</div>--}}
{{--        @if ($errors->any())--}}
{{--            <div class="alert alert-danger">--}}
{{--                <ul>--}}
{{--                    @foreach ($errors->all() as $error)--}}
{{--                        <li>{{ $error }}</li>--}}
{{--                    @endforeach--}}
{{--                </ul>--}}
{{--            </div>--}}
{{--        @endif--}}

{{--        {!! Form::open(['route' => 'form.store']) !!}--}}
{{--        <div class="row justify-content-center">--}}
{{--            <div class="form-group col-6">--}}
{{--                {!! Form::label('email', 'Email') !!}--}}
{{--                {!! Form::email('email', null, ['class' => 'form-control', 'placeholder' => 'Voer het email adres waar je jouw bevestiging wilt ontvangen...']) !!}--}}
{{--            </div>--}}
{{--        </div>--}}

{{--        <!--Blade template-->--}}

{{--        <div class="row">--}}
{{--            <div class="form-group col-4">--}}
{{--                {!! Form::label('user_id', 'Naam') !!}--}}
{{--                {!! Form::select('user_id', $users->pluck('name', 'id'), null, ['class' => 'form-control', 'placeholder' => 'Selecteer de naam van de winnaar...']) !!}--}}
{{--            </div>--}}
{{--            <div class="form-group col-4">--}}
{{--                {!! Form::label('game_id', 'Spel') !!}--}}
{{--                {!! Form::select('game_id', $games->pluck('title', 'id'), null, ['class' => 'form-control', 'placeholder' => 'Selecteer het spel dat is gespeeld...']) !!}--}}
{{--            </div>--}}
{{--            <div class="form-group col-4">--}}
{{--                {!! Form::label('bar_id', 'bar') !!}--}}
{{--                {!! Form::select('bar_id', $bar->pluck('naam', 'id'), null, ['class' => 'form-control', 'placeholder' => 'Selecteer de bar waar je nu speelt']) !!}--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    <div class="row justify-content-center">--}}
{{--        <div class="form-group col-6">--}}
{{--            {!! Form::label('amount_users', 'Aantal Spelers') !!}--}}
{{--            {!! Form::number('amount_users', null, ['class' => 'form-control','placeholder' => 'Selecteer het aantal spelers die mee hebben gespeeld...', 'min' => 1, 'max' => 10]) !!}--}}
{{--        </div>--}}
{{--    </div>--}}

{{--        <div class="row justify-content-center">--}}
{{--            <div class="col text-center">--}}
{{--            <br>--}}
{{--            {!! Form::submit('Send', ['class' => 'btn btn-primary']) !!}--}}
{{--            {!! Form::close() !!}--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--@endsection--}}
