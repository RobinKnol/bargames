{{--This page will display all the current events going on as well as news items--}}
@extends('layouts.master')
@extends('layouts.BottomBar')

@section('content')

    {{--    Background--}}
    <div class="Background-Home"></div>


    {{--@include uitzoeken.--}}

<div class="container container-fluid ">
{{--    Link to the QR-scanner--}}
    <div id="Home1" class="row justify-content-center">
        <div class="col col-md-auto col-sm-auto col-xs-auto">
            <div class="title">
                <h1 class="text-center">Welcome to Bargames!</h1>
                <p class="text-center">Want to play a game?</p>
                <div id="stuff" class="d-grid gap-2 col-6 mx-auto">
                    <button class="btn btn-success" type="button">Open the QR-scanner</button>
                </div>
            </div>
        </div>
    </div>
{{--    Carousel--}}
    <div id="Home" class="row justify-content-center">
        <div class="col col-md-8 col-sm-10 col-xs-6">
            <h2 class="text-center">Nieuws:</h2>
            <div id="carouselExampleIndicators" class="carousel carousel-dark slide" data-bs-ride="carousel">
                {{--                  Buttons for left and right--}}
                <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="visually-hidden">Previous</span>
                </button>
                <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="visually-hidden">Next</span>
                </button>
                {{-- In this foreach loop i want to put all the events--}}
                @foreach($Events as $Event)
                    <div class="carousel-inner" >
                        <div class="carousel-item @if($loop->first) active @endif ">
                            <div class="slider-image text-center">
                                <img src="{{ Storage::url($Event->image) }}" class="d-inline-block border text-center rounded" style="width:100%; max-height:414px;" alt="{{ Storage::url($Event->image) }}">
                {{--   Captions for event items. needs to be done in foreach loop.--}}
                                <div id="nieuws" class="carousel-caption d-none d-md-block">
{{--                                    <a  href="{{ route('bar_detail', [ 'id'=>$Bar->id ]) }}" class="btn btn-link" style="margin-bottom: 10px;">{{ $Bar->naam }}</a><br>--}}
                                    <p class="crop" style="display:inline-block;">{{ $Event->description }}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>

        </div>

    </div>
{{--A more chill look at all the events currently going on--}}
    <div id="Home1" class="row justify-content-center text-center">
        <h2>All events:</h2>
        @foreach( $Events as $Event)
        <div id="Home-card" class="card" style="width: 18rem;">
            <img src="{{ Storage::url($Event->image) }}" class="card-img-top" alt="{{ Storage::url($Event->image) }}">
            <div class="card-body">
                <h5 class="card-title"> {{ $Event->naam }}</h5>
                <p class="card-text">{{ $Event->details }}</p>
            </div>
        </div>
        @endforeach

{{--        <div class="col-sm-6 col-md-6">--}}
{{--            <div id="Home-card" class="card">--}}
{{--                <div class="card-body">--}}
{{--                    <h5 class="card-title"> {{ $Event->naam }}</h5>--}}
{{--                    <p class="card-text">{{ $Event->details }}</p>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--        --}}
    </div>
</div>


@endsection
