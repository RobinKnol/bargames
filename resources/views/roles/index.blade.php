@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    <h1>roles</h1>
                <a class="btn btn-outline-info btn-sm" href="{{ route('roles.create') }}">make role</a>
                </div>
                    <div class="card-body">
                    <ul class="list-group">
                        @foreach($roles as $role)
                            <li class="list-group-item" style="list-style: none;">


                                <form style="display: inline-block; float: right" action="{{ route('roles.destroy', $role->id) }}" method="post">
                                    @csrf
                                    @method('delete')
                                    <button class="btn btn-outline-danger btn-sm float-end m-1" type="submit"><i class="fa-solid fa-trash-can"></i></button>
                                </form>
                                <a href="{{ route('roles.show', $role->id) }}">{{ $role->title }}</a>
                                <a class="btn btn-outline-info btn-sm float-end m-1" href="{{ route('roles.edit', $role->id) }}"><i class="fa-solid fa-wrench"></i></a>
                            </li>
                        @endforeach
                    </ul>
                </div>

            </div>

        </div>

    </div>
{{--    <strong>{{$role->title}}</strong>  <br>--}}
{{--    @foreach($role->userRole as $user)--}}
{{--        &nbsp;{{$user->name}} <br>--}}
{{--    @endforeach--}}

</div>
@endsection
