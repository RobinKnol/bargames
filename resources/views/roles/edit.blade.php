@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header"><h1>roles</h1></div>

                <div class="card-body">
                    <form  action="{{ route('roles.update', $role->id) }}"
                        method="post">
                        @csrf
                        @method('patch')
                    <label for="title">rolnaam</label>
                    <input type="text" name="title" id="title" value="{{ $role->title }}">
                    <button class="btn btn-outline-primary btn-sm" type="submit">update</button>

                    </form>
                </div>

            </div>

        </div>

    </div>
{{--    <strong>{{$role->title}}</strong>  <br>--}}
{{--    @foreach($role->userRole as $user)--}}
{{--        &nbsp;{{$user->name}} <br>--}}
{{--    @endforeach--}}

</div>
@endsection
