{{--This file will display individual pages for each game with more detail that the tiles in Games.blade based on game ID--}}
@extends('layouts.master')
@extends('layouts.BottomBar')

@section('content')

    {{--    Background--}}
<div class="Background-Home"></div>

{{--    actual content--}}
<div class="container">
    <div id="User" class="row">
        <h1 class="text-center Userheader">{{ $TabletopGame->title }}</h1>{{--finding the title--}}
        <hr style="height:2px;max-width: inherit; color:gray;background-color:gray">
        <div class="col">
            <div class="card">
                <div class="row g-0">
                    <div class="col col-sm-3 col-md-3">
                        <img src="{{ Storage::url($TabletopGame->image) }}" class="img-fluid rounded-start" alt="{{ Storage::url($TabletopGame->image) }}"> {{--finding the image from database--}}
                    </div>
                    <div class="col-md-6">
                        <div class="card-body">
                            <h5 class="card-title">{{ $TabletopGame->title }}</h5> {{--finding the title from database--}}
                            <hr style="height:2px;max-width: inherit; color:gray;background-color:gray">
                            <p class="card-text">{{ $TabletopGame->description }}</p> {{--finding the description from database--}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="Game" class="row text-center">
        <div class="col">
            <div id="loginButton">
                <a class="btn btn-lg btn-success" href="/form" role="button"><i class="fa-solid fa-beer-mug-empty"></i> Play a game! <i class="fa-solid fa-beer-mug-empty"></i></a>
            </div>
        </div>
    </div>

{{--    link to the QR scanner for individual games--}}
    <div id="Home1" class="row justify-content-center">
        <div class="col col-md-auto col-sm-auto col-xs-auto">
            <div class="title">
                <p class="text-center">want to start a game? scan the QR-code!</p>
                <div id="stuff" class="d-grid gap-2 col-6 mx-auto">
                    <a href="/scan" class="btn btn-success">Or scan a QR-code</a>
                </div>
            </div>
        </div>
    </div>
</div>



@endsection

