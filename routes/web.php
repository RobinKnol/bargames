<?php

use Illuminate\Support\Facades\Route;
use Carbon\Carbon;
use App\Models\Bar;
use App\Models\TabletopGame;
use App\Http\Controllers\BarsController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//resources
Route::resource('roles',App\Http\Controllers\RoleController::class);
Route::resource('tabletopgames',App\Http\Controllers\TabletopGameController::class);
Route::resource('slider', App\Http\Controllers\SliderController::class);
Route::resource('/form',App\Http\Controllers\FormController::class);



//public get

Route::get('/', function () {
    return view('auth.login');
});
Route::get('/Register', function() {
    return view('auth.register');
});
Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/profile/{id}',[App\Http\Controllers\ProfileController::class, 'user_detail'])->name('user_detail');
Route::get('/Bars/{id}',[App\Http\Controllers\BarController::class, 'bar_detail'])->name('bar_detail');
Route::get('/Games/{id}',[App\Http\Controllers\TabletopGameController::class, 'game_detail'])->name('game_detail');
Route::get('/scan', App\Http\Controllers\ScanController::class . '@scan')->name('scan');
Route::get('/Bars',[App\Http\Controllers\BarController::class, 'Bars'])->name('Bars');
Route::get('/nieuws',[App\Http\Controllers\BarController::class, 'nieuws'])->name('nieuws');
Route::get('/Users',[App\Http\Controllers\ProfileController::class, 'Userlist'])->name('profile');
Route::get('/spellen', [App\Http\Controllers\HomeController::class, 'spellen'])->name('spellen');
Route::get('/gen', App\Http\Controllers\GenerateController::class . '@gen')->name('gen');
Route::get('/Leaderboard',[App\Http\Controllers\LeaderboardController::class, 'Leaderboard'])->name('Leaderboard');

//still need changes


//Route::get('/LeaderBoard', function () {
//    return view('Leaderboard');
//});
Route::get('Games', function () {
    $TabletopGames = TabletopGame::all();
    return view('/Games', compact('TabletopGames'));
});
//end changes

//Auth
Auth::routes();

//Get
Route::get('User', function () {
    return view('userProfile');
});

Route::get('/apicall', function () {
//    lezen van xml file in public
//        $xml = simplexml_load_file('games.xml');
//echo $xml;
    $xmlString = file_get_contents(public_path('games.xml'));

    $xmlObject = simplexml_load_string($xmlString);

    $json = json_encode($xmlObject);

    $games = json_decode($json, true);

    foreach ($games['item'] as $game) {
//    echo "<a href='https://boardgamegeek.com/xmlapi/boardgame/"
//        . $game['@attributes']['objectid']"
//        .</a>"<br>";
        echo "<a href='/apicall/" . $game['@attributes']['objectid'] . "'> " . $game['@attributes']['objectid'] . "</a><br>";

//    <a href='https://boardgamegeek.com/xmlapi/boardgame/
        echo "<img src='" . $game['thumbnail'] . "'><br>";
        echo $game['name'] . "<br>";
    }
});

Route::get('/apicall/{id}', function () {
    $uri = 'https://boardgamegeek.com/xmlapi/boardgame/' . trim($_SERVER['REQUEST_URI'], '/apicall/');
//    die(var_dump($uri));
//    vertaling
    $xmlString = file_get_contents($uri);

    $xmlObject = simplexml_load_string($xmlString);

    $json = json_encode($xmlObject);

    $game = json_decode($json, true);
    echo $game['boardgame']['description'];
});

