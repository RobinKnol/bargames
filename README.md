![error with image](https://cdn.discordapp.com/attachments/1039626826118607031/1062653272697094154/999-footer-logo.png)
## About Bargames
### Installation

First unpack the website using 7zip or winrar (or any other alternative that supports it) and put it in the desired directory.

alternatively for more advanced users follow the following steps:
- cloning the project
  1. Head to the directory you wish to store the project, in the dorectory URL type "cmd" and hit enter, or, if you have bash installed right mouse button on the empty folder and press "git bash here".
  2. type/copy the command:<br>
  ```git clone https://gitlab.com/bargamegroups/bargames02/bargames/-/tree/main```


We use a package that requires installation, to save you some time, just copy and paste the following command:<br>
```npm install html5-qrcode```

## How to run the project
grab a terminal and type/paste the following commands 1 by 1 to make sure the latest dependencies are installed:

```composer install```<br>
```composer require spatie/laravel-permission```<br>
```composer require filament/filament```<br>
```php artisan migrate```<br>
```npm install && npm run dev```


grab another terminal and type:<br>
```php artisan serve```


## How to use

- **Admin Panel:**
    - Manage Admin accounts.
    - Manage user accounts.
    - Manage bars.
    - Manage games.

---

- **How to play:**
    - Select what bar you are at.
    - Select what game you want to play.
    - select with how many people you play.
    - at the end select who won and lost.
    - hit submit and play another, serve the winner a free drink or something.

## Contributors
### This project was made by:


> [**Faisal Koulej**](https://gitlab.com/FaissK)

> [**Robin Knol**](https://gitlab.com/RobinKnol)

> [**Robin Kaarsgaren**](https://gitlab.com/RobProgramming)

> [**Ryan Hira**](https://gitlab.com/RyanHira)

> [**Jaymian Nawang**](https://gitlab.com/jayfr)

