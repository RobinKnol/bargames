<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ScorebordSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('scorebords')->insert([
            'id' => Str::random(10),
            'title' => Str::random(10),
            'Regio' => Str::random(10),
            'score' => Int::random(10),
        ]);

    }
}
