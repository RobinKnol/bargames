<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use App\Models\Profile;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(RolesAndPermissionsSeeder::class);
//        \App\Models\User::factory()->create([
//            'name' => 'admin',
//            'email' => 'admin@admin.nl',
//            'password'=>'password',
//            'role_id' => 3
//        ]);
//        \App\Models\Profile::factory(10)->create();
//        \App\Models\Role::factory(3)->create();
//        \App\Models\Bar::factory(5)->create();
    }
}

