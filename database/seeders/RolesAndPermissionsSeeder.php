<?php

namespace Database\Seeders;

use App\Models\User;
use App\Models\Event;
use App\Models\Bar;
use App\Models\TabletopGame;
use Illuminate\Support\Str;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\PermissionRegistrar;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;


class RolesAndPermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // reset cached roles and permissions
        app()[\Spatie\Permission\PermissionRegistrar::class]->forgetCachedPermissions();

        // Misc basic perms
        $miscPermission = Permission::create(['name' => 'N/A']);

        // USER MODEL crud voor users
        $userPermission1 = Permission::create(['name' => 'create: user']);
        $userPermission2 = Permission::create(['name' => 'read: user']);
        $userPermission3 = Permission::create(['name' => 'update: user']);
        $userPermission4 = Permission::create(['name' => 'delete: user']);

        // ROLE MODEL admin crud voor role
        $rolePermission1 = Permission::create(['name' => 'create: role']);
        $rolePermission2 = Permission::create(['name' => 'read: role']);
        $rolePermission3 = Permission::create(['name' => 'update: role']);
        $rolePermission4 = Permission::create(['name' => 'delete: role']);

        // PERMISSION MODEL admin perms crud
        $permission1 = Permission::create(['name' => 'create: permission']);
        $permission2 = Permission::create(['name' => 'read: permission']);
        $permission3 = Permission::create(['name' => 'update: permission']);
        $permission4 = Permission::create(['name' => 'delete: permission']);

        // PERMISSION MODEL events perms crud
        $eventPermission1 = Permission::create(['name' => 'create: event']);
        $eventPermission2 = Permission::create(['name' => 'read: event']);
        $eventPermission3 = Permission::create(['name' => 'update: event']);
        $eventPermission4 = Permission::create(['name' => 'delete: event']);

        // PERMISSION MODEL bar perms crud
        $barPermission1 = Permission::create(['name' => 'create: bar']);
        $barPermission2 = Permission::create(['name' => 'read: bar']);
        $barPermission3 = Permission::create(['name' => 'update: bar']);
        $barPermission4 = Permission::create(['name' => 'delete: bar']);

        // PERMISSION MODEL tabletopgame perms crud
        $tabletopgamePermission1 = Permission::create(['name' => 'create: tabletopgame']);
        $tabletopgamePermission2 = Permission::create(['name' => 'read: tabletopgame']);
        $tabletopgamePermission3 = Permission::create(['name' => 'update: tabletopgame']);
        $tabletopgamePermission4 = Permission::create(['name' => 'delete: tabletopgame']);

        // ADMINS
        $adminPermission1 = Permission::create(['name' => 'read: admin']);
        $adminPermission2 = Permission::create(['name' => 'update: admin']);

        // CREATE ROLES
        $userRole = Role::create(['name' => 'user'])->syncPermissions([
            $miscPermission,
        ]);


        $adminRole = Role::create(['name' => 'admin'])->syncPermissions([
            $userPermission1,
            $userPermission2,
            $userPermission3,
            $userPermission4,
            $rolePermission1,
            $rolePermission2,
            $rolePermission3,
            $rolePermission4,
            $permission1,
            $permission2,
            $permission3,
            $permission4,
            $adminPermission1,
            $adminPermission2,
            $userPermission1,
            $tabletopgamePermission1,
            $tabletopgamePermission2,
            $tabletopgamePermission3,
            $tabletopgamePermission4,
            $barPermission1,
            $barPermission2,
            $barPermission3,
            $barPermission4,
            $eventPermission1,
            $eventPermission2,
            $eventPermission3,
            $eventPermission4,
        ]);
      $barRole = Role::create(['name' => 'bar'])->syncPermissions([
          $barPermission1,
          $barPermission2,
          $barPermission3,
          $barPermission4,
          $eventPermission1,
          $eventPermission2,
          $eventPermission3,
          $eventPermission4,
          $userPermission2,
          $tabletopgamePermission1,
          $tabletopgamePermission2,
          $adminPermission1,


        ]);
        //speciale perms geven aan een user

        // CREATE ADMINS & USERS
        User::create([
            'name' => 'admin',
            'is_admin' => 1,
            'email' => 'admin@admin.com',
            'email_verified_at' => now(),
            'password' => Hash::make('password'),
            'remember_token' => Str::random(10),
        ])->assignRole($adminRole);

        User::create([
            'name' => 'bar',
            'is_admin' => 1,
            'email' => 'bar@admin.com',
            'email_verified_at' => now(),
            'password' => Hash::make('password'),
            'remember_token' => Str::random(10),
        ])->assignRole($barRole);

        for ($i=1; $i < 10; $i++) {
            User::create([
                'name' => 'Test '.$i,
                'is_admin' => 0,
                'email' => 'test'.$i.'@test.com',
                'email_verified_at' => now(),
                'password' => Hash::make('password'), // password
                'remember_token' => Str::random(10),
            ])->assignRole($userRole);
        }
    }
}
