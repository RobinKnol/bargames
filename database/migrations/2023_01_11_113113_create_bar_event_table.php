<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bar_event', function (Blueprint $table) {
            $table->id();
            $table->unsignedBiginteger('bar_id')->unsigned();
            $table->unsignedBiginteger('event_id')->unsigned();
            $table->foreign('bar_id')->references('id')
                ->on('bars')->onDelete('cascade');
            $table->foreign('event_id')->references('id')
                ->on('events')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bar_event');
    }
};
