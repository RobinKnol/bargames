<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Tabletopgame>
 */
class TabletopgameFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'title' => fake()->country(),
            'image' => fake()->imageUrl(),
            'description' => fake()->text(),
//            'score' => random_int(1,3),
        ];
    }
}

