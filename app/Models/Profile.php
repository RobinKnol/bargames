<?php

namespace App\Models;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    use HasFactory;
//many-to-many for users
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    //getting information from database
    public function Users(){
        $Users = User::all();

        return view('/Userlist', compact('Users'));
    }
}
