<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    use HasFactory;
    protected $fillable = [
        'naam',
        'details',
        'image'
    ];
//many-to-many for bars
    public function bar()
    {
        return $this->belongsToMany(Bar::class);
    }
}
