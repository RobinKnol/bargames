<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


class Bar extends Model
{
    use HasFactory;

    protected $fillable = [
        'naam',
        'description',
        'locatie',
        'image'
    ];

    public function Barprofile(){
        $Bars = Bar::all();
        $TabletopGame = TabletopGame::all();
        return view('/Barprofile', compact('Bars', 'TabletopGame'));
    }

    //many-to-many for TabletopGame
    public function tabletopgame()
    {
        return $this->belongsToMany(TabletopGame::class);
    }

    //many-to-many for Users
    public function baruser()
    {
        return $this->belongsToMany(User::class);
//        , 'bar_user', 'bar_id', 'user_id'
    }
    //many-to-many for events
    public function Event()
    {
        return $this->belongsToMany(Event::class);
    }
}
