<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Filament\Models\Contracts\FilamentUser;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable implements FilamentUser
{
    use HasApiTokens, HasFactory, Notifiable;
    use HasRoles;
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'is_admin',
        'city',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function profile()
    {
        return $this->hasOne(Profile::class);
    }

//    public function role()
//    {
//        return $this->belongsTo(Role::class);
//    }


    public function games()
    {
        return $this->belongsToMany(TabletopGame::class);

    }

    public function bars()
    {
        return $this->belongsToMany(Bar::class);

    }

    public function role()
    {
        return $this->belongsToMany(Role::class);
    }
//user kan niet bij de adminPanel komen
    public function canAccessFilament(): bool
    {
        return str_ends_with($this->email, '@admin.com');
    }
    public function leaderboard()
    {
        return $this->belongsToMany(Leaderboard::class);
    }

}
