<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
class TabletopGame extends Model
{
    use HasFactory;

    protected $fillable = [
        'title', 'image', 'description'
    ];
//many-to-many
    public function Bar()
    {
        return $this->belongsToMany(Bar::class);
    }

}
