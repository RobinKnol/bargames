<?php

namespace App\Filament\Resources\BarResource\Pages;

use App\Filament\Resources\BarResource;
use Filament\Pages\Actions;
use Filament\Resources\Pages\CreateRecord;

class CreateBar extends CreateRecord
{
    protected static string $resource = BarResource::class;

    protected function getRedirectUrl(): string
    {
        return $this->getResource()::getUrl('index');
    }

}
