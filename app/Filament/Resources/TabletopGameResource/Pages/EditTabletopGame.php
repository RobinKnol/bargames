<?php

namespace App\Filament\Resources\TabletopGameResource\Pages;

use App\Filament\Resources\TabletopGameResource;
use Filament\Pages\Actions;
use Filament\Resources\Pages\EditRecord;

class EditTabletopGame extends EditRecord
{
    protected static string $resource = TabletopGameResource::class;

    protected function getActions(): array
    {
        return [
            Actions\DeleteAction::make(),
        ];
    }

    protected function getRedirectUrl(): string
    {
        return $this->getResource()::getUrl('index');
    }

}
