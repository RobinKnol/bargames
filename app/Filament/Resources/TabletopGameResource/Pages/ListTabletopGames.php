<?php

namespace App\Filament\Resources\TabletopGameResource\Pages;

use App\Filament\Resources\TabletopGameResource;
use Filament\Pages\Actions;
use Filament\Resources\Pages\ListRecords;

class ListTabletopGames extends ListRecords
{
    protected static string $resource = TabletopGameResource::class;

    protected function getActions(): array
    {
        return [
            Actions\CreateAction::make(),
        ];
    }
}
