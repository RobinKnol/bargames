<?php

namespace App\Filament\Resources\TabletopGameResource\Pages;

use App\Filament\Resources\TabletopGameResource;
use Filament\Pages\Actions;
use Filament\Resources\Pages\CreateRecord;

class CreateTabletopGame extends CreateRecord
{
    protected static string $resource = TabletopGameResource::class;

    protected function getRedirectUrl(): string
    {
        return $this->getResource()::getUrl('index');
    }

}
