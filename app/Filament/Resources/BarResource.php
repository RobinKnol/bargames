<?php

namespace App\Filament\Resources;

use App\Filament\Resources\BarResource\Pages;
use App\Filament\Resources\BarResource\RelationManagers;
use App\Models\Bar;
use App\Models\TabletopGame;
use Filament\Forms;
use Filament\Resources\Form;
use Filament\Resources\Resource;
use Filament\Resources\Table;
use Filament\Tables;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletingScope;

class BarResource extends Resource
{
    protected static ?string $model = Bar::class;

    protected static ?string $navigationIcon = 'heroicon-o-collection';


    public static function form(Form $form): Form
    {
        return $form
            ->schema([
                Forms\Components\TextInput::make('naam')->required(),
                Forms\Components\TextInput::make('locatie')->required(),
//                Forms\Components\TextInput::make('regio')->required(),
                Forms\Components\TextInput::make('description')->required(),
                Forms\Components\FileUpload::make('image')->image(),
                Forms\Components\Select::make('tabletopGame')
                    ->relationship('TabletopGame','name')
                    ->label('tabletop_game_id')
                    ->multiple()
                    ->options(TabletopGame::all()->pluck('title', 'id')->toArray())
                    ->reactive(),
//                Forms\Components\Select::make('Events')
//                    ->relationship('Event','name')
//                    ->label('Event_id')
//                    ->multiple()
//                    ->options(TabletopGame::all()->pluck('naam', 'id')->toArray())
//                    ->reactive(),
                ]);
    }

    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                Tables\Columns\TextColumn::make('naam')
                    ->sortable()
                    ->searchable(),
                Tables\Columns\TextColumn::make('locatie')
                    ->sortable()
                    ->searchable(),
//                Tables\Columns\TextColumn::make('regio')
//                    ->sortable()
//                    ->searchable(),
                Tables\Columns\TextColumn::make ('description'),
                Tables\Columns\ImageColumn::make('image')->square(),
                Tables\Columns\TextColumn::make('tabletopGame.title'),
//                Tables\Columns\TextColumn::make('bar.naam'),
//                Tables\Columns\textColumn::make('TabletopGame'),
            ])
            ->filters([
                //
            ])
            ->actions([
                Tables\Actions\EditAction::make(),
            ])
            ->bulkActions([
                Tables\Actions\DeleteBulkAction::make(),
            ]);
    }

    public static function getRelations(): array
    {
        return [
            //
        ];
    }

    public static function getPages(): array
    {
        return [
            'index' => Pages\ListBars::route('/'),
            'create' => Pages\CreateBar::route('/create'),
            'edit' => Pages\EditBar::route('/{record}/edit'),
        ];
    }
}
