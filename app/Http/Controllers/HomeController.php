<?php

namespace App\Http\Controllers;

use App\Models\Bar;
use App\Models\Role;
use App\Models\TabletopGame;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\BarController;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $games = TabletopGame::paginate(4);
        $users = User::all();
        $roles = Role::all();
        $bars = Bar::all();
        return view('home',compact('roles', 'bars', 'games', 'users'));
    }



    public function spellen()
    {
        $bars = Bar::all();
        $game = TabletopGame::all();
        return view('spellen', compact('bars', 'game'));
    }

    public function bar_detail ($id) {
        $Bar = Bar::find($id);
        if (is_null($Bar)){
            return redirect('/404');
        }
        return view('spellen', compact('Bar',));
    }

}

