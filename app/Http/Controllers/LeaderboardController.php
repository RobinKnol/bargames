<?php
namespace App\Http\Controllers;

use App\Models\Leaderboard;
use App\Models\User;
use App\Models\Bar;
use App\Models\TabletopGame;
use App\Http\Requests\StoreLeaderboardRequest;
use App\Http\Requests\UpdateLeaderboardRequest;

class LeaderboardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function Leaderboard()
    {
        //
//        $users = User::orderByRaw('CONVERT(wins, SIGNED) desc');
        $users = User::orderBy('wins', 'desc')->paginate(15);
        $bars = Bar::all();
        $game = TabletopGame::orderByRaw('CONVERT(wins, SIGNED) desc')->get();
        return view('/Leaderboard', compact('users', 'bars', 'game'));
    }
    public function index(Request $request)
    {
        $city = $request->input('city');
        $users = User::where('city', $city)->get();
        $winners = $users->sortByDesc('wins')->take(10);
        return view('leaderboard.index', compact('winners'));
    }
    public function localLeaderboard($city)
    {
        $user = User::where('city', $city)->get();
        return view('leaderboard.local', compact('user'));
    }
//    public function cityLeaderboard($city)
//    {
//        $users = User::where('city', $city)->orderBy('wins', 'desc')->get();
//        return view('leaderboard.city', compact('users'));
//    }


}
