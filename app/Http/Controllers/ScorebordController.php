<?php

namespace App\Http\Controllers;

use App\Models\Bar;
use App\Models\Scorebord;
use App\Http\Requests\StoreScorebordRequest;
use App\Http\Requests\UpdateScorebordRequest;

class ScorebordController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreScorebordRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreScorebordRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Scorebord  $scorebord
     * @return \Illuminate\Http\Response
     */
    public function show(Scorebord $scorebord)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Scorebord  $scorebord
     * @return \Illuminate\Http\Response
     */
    public function edit(Scorebord $scorebord)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateScorebordRequest  $request
     * @param  \App\Models\Scorebord  $scorebord
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateScorebordRequest $request, Scorebord $scorebord)
    {
        //

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Scorebord  $scorebord
     * @return \Illuminate\Http\Response
     */
    public function destroy(Scorebord $scorebord)
    {
        //

    }
}
