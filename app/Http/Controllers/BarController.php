<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreBarRequest;
use App\Models\Bar;
use App\Models\TabletopGame;
use App\Models\Event;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class BarController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $bar = Bar::all();
        return view('bars.index', compact('bar'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $tabletopgame = TabletopGame::all();
        return view('bars.create', compact('tabletopgame'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreBarRequest $request)
    {
        $image = $request->file('image')->store('public/images/');

        $bar = Bar::create([
            'naam' => $request->naam,
            'locatie' => $request->locatie,
            'description' => $request->description,
            'image' => $image

        ]);

        if ($request->has('tabletopgame')) {
            $bar->tabletopgame()->attach($request->tabletopgame);
        }

        return to_route('bars.index')->with('success', 'Bar created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\Bar $bars
     * @return \Illuminate\Http\Response
     */
    public function show(Bar $bar)
    {
        return view('bars.show', compact('bar'));
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Bar $bar)
    {
        $tabletopgame = TabletopGame::all();
        return view('bars.edit', compact('bar', 'tabletopgame'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Bar $bar)
    {
        $request->validate([
            'naam' => 'required',
            'locatie' => 'required',
            'description' => 'required'

        ]);
        $image = $bar->image;
        if ($request->hasFile('image')) {
            Storage::delete($bar->image);
            $image = $request->file('image')->store('public/images/');
        }

        $bar->update([
            'naam' => $request->naam,
            'locatie' => $request->locatie,
            'description' => $request->description,
            'image' => $image

        ]);

        if ($request->has('tabletopgame')) {
            $bar->tabletopgame()->sync($request->tabletopgame);
        }
        return to_route('bars.index')->with('success', 'Bar updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Bar $bar)
    {
        Storage::delete($bar->image);
        $bar->tabletopgame()->detach();
        $bar->delete();
        return to_route('bars.index')->with('danger', 'Bar deleted successfully.');
    }

//getting the data from the model.
    public function  nieuws(){
        $Bars = Bar::all();
        $Events = Event::all();
        return view('/nieuws', compact('Bars', 'Events'));
    }

    //getting the data from the model.
    public function Barprofile(){
        $Bars = Bar::all();
        $TabletopGame = TabletopGame::all();
        return view('/Barprofile', compact('Bars', 'TabletopGame'));
    }

    //getting the data from the model.
    public function  Bars(){
        $Bars = Bar::all();
        return view('/Bars', compact('Bars'));
    }
    //getting data based on bar ID and filling the page with information based on the id found.
    public function bar_detail ($id) {
        $Bar = Bar::find($id);
        if (is_null($Bar)){
            return redirect('/404');
        }
        return view('barprofile', compact('Bar',));
    }

}
