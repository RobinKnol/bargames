<?php


namespace App\Http\Controllers;

use App\Models\TabletopGame;
use Illuminate\Http\Request;

class TabletopGameController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tabletopgames = TabletopGame::latest()->paginate(5);

        return view('tabletopgames.index',compact('tabletopgames'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('tabletopgames.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'description' => 'required',
        ]);

        $input = $request->all();

        if ($image = $request->file('image')) {
            $destinationPath = 'images/';
            $profileImage = date('YmdHis') . "Downloads" . $image->getClientOriginalExtension();
            $image->move($destinationPath, $profileImage);
            $input['image'] = "$profileImage";
        }

        TabletopGame::create($input);

        return redirect()->route('tabletopgames.index')
            ->with('success','Tabletop game created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\TabletopGame  $tabletopgame
     * @return \Illuminate\Http\Response
     */
    public function show(TabletopGame $tabletopgame)
    {
        return view('tabletopgames.show',compact('tabletopgame'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\TabletopGame  $tabletopgame
     * @return \Illuminate\Http\Response
     */
    public function edit(TabletopGame $tabletopgame)
    {
        return view('tabletopgames.edit',compact('tabletopgame'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\TabletopGame  $tabletopgame
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, TabletopGame $tabletopgame)
    {
        $request->validate([
            'title' => 'required',
            'description' => 'required',
        ]);

        $input = $request->all();

        if ($image = $request->file('image')) {
            $destinationPath = 'images/';
            $profileImage = date('YmdHis') . "Downloads" . $image->getClientOriginalExtension();
            $image->move($destinationPath, $profileImage);
            $input['image'] = "$profileImage";
        }else{
            unset($input['image']);
        }

        $tabletopgame->update($input);

        return redirect()->route('tabletopgames.index')
            ->with('success','TTG updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\TabletopGame  $tabletopgame
     * @return \Illuminate\Http\Response
     */
    public function destroy(TabletopGame $tabletopgame)
    {
        $tabletopgame->delete();

        return redirect()->route('tabletopgames.index')
            ->with('success','Product deleted successfully');
    }


    //Finding data based on Tabletopgame ID and sending all information that belong to said id to .blade file.
    public function game_detail ($id) {
        $TabletopGame = TabletopGame::find($id);
        if (is_null($TabletopGame)){ //return 404 if theres no information found.
            return redirect('/404');
        }
        return view('Game_detail', compact('TabletopGame',));
    }

}
