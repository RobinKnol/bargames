<?php
namespace App\Http\Controllers;

use App\Models\Form;
use App\Http\Requests\StoreFormRequest;
use App\Http\Requests\UpdateFormRequest;
use App\Models\TabletopGame;
use App\Models\User;
use App\Models\Bar;

class FormController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $games = TabletopGame::all();
//        $users = User::all();
        $bar = bar::all();
        $users = User::orderBy('wins', 'desc')->paginate(15);


        return view('GameSession', compact('games', 'users', 'bar'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreFormRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreFormRequest $request)
    {
        // Validate the form inputs
        $validatedData = $request->validate([
            'user_id' => 'required|string',
            'email' => 'required|email',
            'game_id' => 'required|integer',
            'bar_id' => 'required|integer',
            'winner' => 'required|integer',
            'loser' => 'required|array',

        ]);
        $winner = User::find($request->winner);
        $winner->wins++;
        $winner->save();

        $losers = $request->loser;
        if (is_array($losers)) {
            foreach ($losers as $loser) {
                $l = User::find($loser);
                $l->losses++;
                $l->save();
            }
        }


        // Create a new Form instance
        $form = new Form();

        // Assign the form inputs to the Form instance
        $form->user_id = $validatedData['user_id'];
        $form->email = $validatedData['email'];
        $form->game_id = $validatedData['game_id'];
        $form->bar = $validatedData['bar_id'];

        // Save the winner and loser fields in the Form model
        $form->winner = $winner->id;
        $form->loser = json_encode($losers);

        // Save the Form instance to the database
        $form->save();

        // Flash a success message to the session
        session()->flash('success', 'Form submitted successfully');


        // Redirect or return a response with a success message
        return redirect()->route('form.index');
//        ->with('success', 'Form submitted successfully')
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Form  $form
     * @return \Illuminate\Http\Response
     */
    public function show(Form $form)
    {
        $winner = User::find($form->winner);
        $losers = User::findMany(json_decode($form->loser));
        $Bars = bar::find($form->bar);
        return view('form.show', compact('form', 'winner', 'losers'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Form  $form
     * @return \Illuminate\Http\Response
     */
    public function edit(Form $form)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateFormRequest  $request
     * @param  \App\Models\Form  $form
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateFormRequest $request, Form $form)
    {
        // Validate the form data
        $request->validate([
            'name' => 'required|max:255',
            'email' => 'required|email',
        ]);

        // Process the form data (e.g., store in a database, send an email, etc.)

        // Redirect the user to a confirmation page
        return redirect('/form/confirmation');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Form  $form
     * @return \Illuminate\Http\Response
     */
    public function destroy(Form $form)
    {
        //
    }


}

