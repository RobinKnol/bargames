<?php

namespace App\Http\Controllers;

use App\Models\Profile;
use App\Http\Requests\StoreProfileRequest;
use App\Http\Requests\UpdateProfileRequest;
use App\Models\User;

class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreProfileRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreProfileRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Profile  $profile
     * @return \Illuminate\Http\Response
     */


    public function edit(Profile $profile)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateProfileRequest  $request
     * @param  \App\Models\Profile  $profile
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateProfileRequest $request, Profile $profile)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Profile  $profile
     * @return \Illuminate\Http\Response
     */
    public function destroy(Profile $profile)
    {
        //
    }

// Finding all data from users to later show in the .blade file.
    public function Userlist(){
        $Users = User::all();
        return view('/Userlist', compact('Users'));
    }

//finding users based on ID and sending the .blade file the appropriate information.
    public function user_detail ($id) {
        $User = User::find($id);
        if (is_null($User)){ //if no data is found return them to a 404 page.
            return redirect('/404');
        }
        return view('profile', compact('User',));
    }


    public function show($id)
    {
        // Retrieve the user with the specified ID
        $user = User::find($id); //user class

        // Retrieve all the games played by the user
        $games = Game::where('user_id', $user->id)->get(); //replace Game with whatever class faisal is using to register played games

        //count of total games played by user
        $total_games = $games->count();

        // Initialize variables for counting wins and losses
        $wins = 0;
        $losses = 0;

        // Iterate through the games and count the wins and losses
        foreach ($games as $game) {
            if ($game->win) {
                $wins++;
            } else {
                $losses++;
            }
        }

        // Calculate the win/loss ratio
        $win_loss_ratio = $wins / ($wins + $losses);

        //sign up date of user
        $sign_up_date = $user->Created_at;

        // Pass the user, wins, losses, and win/loss ratio to the view
        return view('profile', compact('user', 'wins', 'losses', 'win_loss_ratio', 'total_games', 'sign_up_date'));

    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Profile  $profile
     * @return \Illuminate\Http\Response
     */
}
