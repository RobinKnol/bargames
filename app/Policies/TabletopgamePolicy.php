<?php

namespace App\Policies;

use Spatie\Permission\Models\Permission;
use Illuminate\Auth\Access\HandlesAuthorization;
use App\Models\User;
use App\Models\TabletopGame;

class TabletopgamePolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any models.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function viewAny(User $user)
    {
        return $user->hasAnyRole(['admin','bar']);
    }

    /**
     * Determine whether the user can create models.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function create(User $user)
    {
        return $user->hasAnyRole(['admin']);
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\Models\User  $user
     * @param  \Spatie\Permission\Models\Permission  $TabletopGame
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function update(User $user, TabletopGame $TabletopGame)
    {
        return $user->hasAnyRole(['admin']);
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\Models\User  $user
     * @param  \Spatie\Permission\Models\Permission  $tabletopGame
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function delete(User $user, TabletopGame $tabletopGame)
    {
        return $user->hasAnyRole(['admin']);
    }

}
