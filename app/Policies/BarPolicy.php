<?php

namespace App\Policies;

use Spatie\Permission\Models\Permission;

use App\Models\Bar;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;



class BarPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any models.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function viewAny(User $user)
    {
        return $user->hasAnyRole(['admin','bar']);
    }

    /**
     * Determine whether the user can create models.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function create(User $user)
    {
        return $user->hasAnyRole(['admin']);
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\Models\User  $user
     * @param  \Spatie\Permission\Models\Permission  $bar
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function update(User $user, Bar $bar)
    {
        return $user->hasAnyRole(['admin']);
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\Models\User  $user
     * @param  \Spatie\Permission\Models\Permission  $bar
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function delete(User $user, Bar $bar)
    {
        return $user->hasAnyRole(['admin']);
    }
}
